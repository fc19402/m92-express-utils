'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _expressHttpContext = _interopRequireDefault(require("express-http-context"));

var _autoBind = _interopRequireDefault(require("auto-bind"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class HttpContext {
  constructor(CONFIG, CONSTANTS) {
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    (0, _autoBind.default)(this);
  } // Session ID


  setSessionId() {
    var sessionId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      SESSION_ID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(SESSION_ID_CONTEXT, sessionId);
  }

  getSessionId() {
    var {
      SESSION_ID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(SESSION_ID_CONTEXT) || '';
  } // Request ID


  setRequestId() {
    var requestId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      REQUEST_ID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(REQUEST_ID_CONTEXT, requestId);
  }

  getRequestId() {
    var {
      REQUEST_ID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(REQUEST_ID_CONTEXT) || '';
  } // Request Token


  setRequestToken() {
    var requestToken = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      REQUEST_TOKEN_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(REQUEST_TOKEN_CONTEXT, requestToken);
  }

  getRequestToken() {
    var {
      REQUEST_TOKEN_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(REQUEST_TOKEN_CONTEXT) || '';
  } // Response Token


  setResponseToken() {
    var responseToken = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      RESPONSE_TOKEN_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(RESPONSE_TOKEN_CONTEXT, responseToken);
  }

  getResponseToken() {
    var {
      RESPONSE_TOKEN_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(RESPONSE_TOKEN_CONTEXT) || '';
  } // User


  setUser() {
    var user = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var {
      USER_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(USER_CONTEXT, user);
  }

  getUser() {
    var {
      USER_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(USER_CONTEXT) || {};
  } // API User


  setApiUser() {
    var apiUser = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var {
      API_USER_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(API_USER_CONTEXT, apiUser);
  }

  getApiUser() {
    var {
      API_USER_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(API_USER_CONTEXT) || {};
  } // UID


  setUid() {
    var uid = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      UID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(UID_CONTEXT, uid);
  }

  getUid() {
    var {
      UID_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(UID_CONTEXT) || '';
  }

  setReqResBodyLog() {
    var reqResBodyLog = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
    var {
      REQ_RES_BODY_LOG_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.set(REQ_RES_BODY_LOG_CONTEXT, reqResBodyLog);
  }

  getReqResBodyLog() {
    var {
      REQ_RES_BODY_LOG_CONTEXT
    } = this.CONSTANTS;
    return _expressHttpContext.default.get(REQ_RES_BODY_LOG_CONTEXT);
  }

}

exports.default = HttpContext;