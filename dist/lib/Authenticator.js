'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _AUTHENTICATOR_ERRORS = _interopRequireDefault(require("../constants/AUTHENTICATOR_ERRORS"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

class Authenticator {
  constructor(CONFIG, CONSTANTS, thisExpressUtils) {
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    var {
      DISABLE_OAUTH,
      DISABLE_AAUTH
    } = CONFIG;
    var {
      HttpClient,
      CustomError,
      reqHandler,
      ERROR_CLASSIFICATION,
      httpContext
    } = thisExpressUtils;
    var {
      asyncWrapper
    } = reqHandler;
    this.ERROR_CLASSIFICATION = ERROR_CLASSIFICATION;
    this.HttpClient = HttpClient;
    this.CustomError = CustomError;
    this.httpContext = httpContext;
    (0, _autoBind.default)(this);
    var validateToken = DISABLE_OAUTH && this._bypassMiddleware || this._validateToken;
    this.validateToken = asyncWrapper(validateToken);
    var validateApiKey = DISABLE_AAUTH && this._bypassMiddleware || this._validateApiKey;
    this.validateApiKey = asyncWrapper(validateApiKey);
  }

  _bypassMiddleware(request, response, next) {
    return _asyncToGenerator(function* () {
      return process.nextTick(next);
    })();
  }

  _validateToken(request, response, next) {
    var _this = this;

    return _asyncToGenerator(function* () {
      // Check if 'byPassOAuth' Property has been Set by Another Middleware to Disable Auth
      var {
        byPassOAuth
      } = request;

      if (byPassOAuth === true) {
        return process.nextTick(next);
      }

      var apiParams = _this._buildOAuthParams(request) || {};
      yield _this._callOAuthApi(apiParams, request, response);
      next();
    })();
  }

  _validateApiKey(request, response, next) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      // Check if 'byPassAAuth' Property has been Set by Another Middleware to Disable Auth
      var {
        byPassAAuth
      } = request;

      if (byPassAAuth === true) {
        return process.nextTick(next);
      }

      var apiParams = _this2._buildAAuthParams(request) || {};
      yield _this2._callAAuthApi(apiParams, request, response);
      next();
    })();
  } // --------- Token Authentication --------------------------------------------


  _buildOAuthParams(request) {
    var {
      CONFIG,
      CONSTANTS,
      CustomError
    } = this;

    var token = this._extractRequestToken(request);

    if (!token) {
      throw new CustomError(undefined, _AUTHENTICATOR_ERRORS.default.TOKEN_NOT_FOUND);
    }

    var {
      OAUTH_POOL_ID = '',
      OAUTH_ENDPOINT = ''
    } = CONFIG;
    var {
      POOL_ID_REQUEST_KEY = ''
    } = CONSTANTS;
    var url = OAUTH_ENDPOINT;
    var poolId = OAUTH_POOL_ID || request[POOL_ID_REQUEST_KEY];
    var body = {
      poolId,
      token
    };
    var oauthParams = {
      url,
      body
    };
    return oauthParams;
  }

  _callOAuthApi(params, routerRequest, routerResponse) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      var {
        url = '',
        headers = {},
        body = {}
      } = params;
      var options = {
        method: 'POST',
        url,
        headers,
        data: body
      };
      var httpClient = new _this3.HttpClient();
      var response = yield httpClient.request(options);
      return _this3._setRouterRequestResponseForOAuth(routerRequest, routerResponse, response);
    })();
  }

  _extractRequestToken(request) {
    var {
      CONSTANTS
    } = this;
    var {
      DEFAULT_AUTH,
      AUTH_HEADER,
      ACCESS_TOKEN_PROP
    } = CONSTANTS;
    var token = DEFAULT_AUTH && request[AUTH_HEADER.REQUEST_PROP] || request[ACCESS_TOKEN_PROP];
    return token;
  }

  _setRouterRequestResponseForOAuth(routerRequest, routerResponse, authResponse) {
    var {
      CONSTANTS,
      httpContext
    } = this;
    var {
      DEFAULT_AUTH,
      AUTH_HEADER,
      ACCESS_TOKEN_PROP,
      CLAIMS_REQUEST_KEY,
      OAUTH_RESPONSE_PROP
    } = CONSTANTS;
    var {
      data: oauthBody = {}
    } = authResponse;
    var {
      statusCode,
      data: oauthData = {}
    } = oauthBody;
    var {
      claims = {},
      refreshedToken
    } = oauthData; // '!statusCode' => response data is not of structure 'ResponseBody'

    if (!statusCode) {
      routerResponse[OAUTH_RESPONSE_PROP] = oauthData;
      return;
    }

    routerRequest[CLAIMS_REQUEST_KEY] = claims;
    var refreshedTokenResponseProp = DEFAULT_AUTH && AUTH_HEADER.REQUEST_PROP || ACCESS_TOKEN_PROP;
    routerResponse[refreshedTokenResponseProp] = refreshedToken;
    httpContext.setResponseToken(refreshedToken);
    httpContext.setUser(claims);
  } // ---------------------------------------------------------------------------
  // --------- Token Authentication --------------------------------------------


  _buildAAuthParams(request) {
    var {
      CONFIG,
      CONSTANTS,
      CustomError
    } = this;

    var apiKey = this._extractApiKey(request);

    if (!apiKey) {
      throw new CustomError(undefined, _AUTHENTICATOR_ERRORS.default.API_KEY_NOT_FOUND);
    }

    var {
      AAUTH_ENDPOINT,
      AAUTH_API_KEY
    } = CONFIG;
    var {
      API_KEY_HEADER_KEY
    } = CONSTANTS;
    var url = AAUTH_ENDPOINT;
    var headers = {
      [API_KEY_HEADER_KEY]: AAUTH_API_KEY
    };
    var body = {
      apiKey
    };
    var aauthParams = {
      url,
      headers,
      body
    };
    return aauthParams;
  }

  _callAAuthApi(params, routerRequest, routerResponse) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      var {
        url = '',
        headers = {},
        body = {}
      } = params;
      var options = {
        method: 'POST',
        url,
        headers,
        data: body
      };
      var httpClient = new _this4.HttpClient();
      var response = yield httpClient.request(options);
      return _this4._setRouterRequestResponseForAAuth(routerRequest, routerResponse, response);
    })();
  }

  _extractApiKey(request) {
    var {
      CONSTANTS
    } = this;
    var {
      API_KEY_REQUEST_KEY
    } = CONSTANTS;
    var apiKey = request[API_KEY_REQUEST_KEY];
    return apiKey;
  }

  _setRouterRequestResponseForAAuth(routerRequest, routerResponse, authResponse) {
    var {
      CONSTANTS,
      httpContext
    } = this;
    var {
      AAUTH_RESPONSE_PROP,
      API_KEY_REQUEST_KEY
    } = CONSTANTS;
    var {
      data: aauthBody = {}
    } = authResponse;
    var {
      statusCode,
      data: aauthData = {}
    } = aauthBody; // '!statusCode' => response data is not of structure 'ResponseBody'

    if (!statusCode) {
      routerResponse[AAUTH_RESPONSE_PROP] = aauthBody;
      return;
    }

    routerRequest[API_KEY_REQUEST_KEY] = aauthData;
    httpContext.setApiUser(aauthData);
  } // ---------------------------------------------------------------------------


}

exports.default = Authenticator;