'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = BaseLogCreator;

var _uuid = require("uuid");

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _chalk = _interopRequireDefault(require("chalk"));

var _LOGGER = _interopRequireDefault(require("../constants/LOGGER"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function BaseLogCreator(CONFIG, CONSTANTS, thisExpressUtils) {
  var {
    httpContext
  } = thisExpressUtils;
  var {
    SERVICE_NAME,
    LOGGER_PROPS_STRNGIFIED
  } = CONFIG;
  var DEFAULT_PROP_LIST = ['service', 'type', 'message', 'level', 'uid', 'sessionId', 'requestId', 'id', 'timestamp', 'timestampUTC', 'data', 'url', 'method', 'statusCode', 'status', 'responseMessage', 'reqBody', 'resBody', 'httpVersion', 'ipAddress', 'userAgent', 'message', 'responseTimeInMS'];
  return class BaseLog {
    constructor(type, level) {
      this.id = (0, _uuid.v4)();
      this.service = SERVICE_NAME || '';
      this.type = type || '';
      this.uid = httpContext.getUid();
      this.sessionId = httpContext.getSessionId();
      this.requestId = httpContext.getRequestId();
      this.level = level || '';
      var timestamp = (0, _momentTimezone.default)();
      this.timestamp = timestamp.format('x');
      this.timestampUTC = timestamp.format('YYYY-MM-DD HH:mm:ss.SSS Z');
      this.data = {};
      this.url = '';
      this.method = '';
      this.statusCode = 0;
      this.status = '';
      this.responseMessage = '';
      this.httpVersion = '';
      this.ipAddress = '';
      this.userAgent = '';
      this.message = '';
      this.responseTimeInMS = 0;
      this.reqBody = {};
      this.resBody = {};
      (0, _autoBind.default)(this);
    }

    print() {
      var fullObject = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var {
        timestamp,
        type,
        level,
        message
      } = this;
      var logColor = _LOGGER.default.LEVEL_COLORS[level];
      var logMsg = "".concat(timestamp, " | ").concat(level || type, " | ").concat(message);
      logMsg = _chalk.default[logColor] && _chalk.default[logColor](logMsg) || logMsg;
      var log = this.getLog();
      log = JSON.stringify(log);
      log = _chalk.default[logColor] && _chalk.default[logColor](log) || log;
      var printMsg = fullObject && log || logMsg;
      process.stdout.write("".concat(printMsg, "\n"));
    }

    getLog() {
      var log = DEFAULT_PROP_LIST.reduce((obj, prop) => {
        obj[prop] = this[prop];
        return obj;
      }, {});

      if (LOGGER_PROPS_STRNGIFIED) {
        log.data = JSON.stringify(log.data);
        log.reqBody = JSON.stringify(log.reqBody);
        log.resBody = JSON.stringify(log.resBody);
      }

      return log;
    }

    static get CONFIG() {
      return CONFIG;
    }

    static get CONSTANTS() {
      return CONSTANTS;
    }

  };
}