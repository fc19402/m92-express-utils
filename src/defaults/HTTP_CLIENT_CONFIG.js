'use strict'

const HTTP_CLIENT_CONFIG = {
  timeout: 30000
}

export default HTTP_CLIENT_CONFIG
