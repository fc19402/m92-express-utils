'use strict'

import httpContext from 'express-http-context'
import autoBind from 'auto-bind'

export default class HttpContext {
  constructor (CONFIG, CONSTANTS) {
    this.CONFIG = CONFIG
    this.CONSTANTS = CONSTANTS

    autoBind(this)
  }

  // Session ID
  setSessionId (sessionId = '') {
    const { SESSION_ID_CONTEXT } = this.CONSTANTS
    return httpContext.set(SESSION_ID_CONTEXT, sessionId)
  }

  getSessionId () {
    const { SESSION_ID_CONTEXT } = this.CONSTANTS
    return httpContext.get(SESSION_ID_CONTEXT) || ''
  }

  // Request ID
  setRequestId (requestId = '') {
    const { REQUEST_ID_CONTEXT } = this.CONSTANTS
    return httpContext.set(REQUEST_ID_CONTEXT, requestId)
  }

  getRequestId () {
    const { REQUEST_ID_CONTEXT } = this.CONSTANTS
    return httpContext.get(REQUEST_ID_CONTEXT) || ''
  }

  // Request Token
  setRequestToken (requestToken = '') {
    const { REQUEST_TOKEN_CONTEXT } = this.CONSTANTS
    return httpContext.set(REQUEST_TOKEN_CONTEXT, requestToken)
  }

  getRequestToken () {
    const { REQUEST_TOKEN_CONTEXT } = this.CONSTANTS
    return httpContext.get(REQUEST_TOKEN_CONTEXT) || ''
  }

  // Response Token
  setResponseToken (responseToken = '') {
    const { RESPONSE_TOKEN_CONTEXT } = this.CONSTANTS
    return httpContext.set(RESPONSE_TOKEN_CONTEXT, responseToken)
  }

  getResponseToken () {
    const { RESPONSE_TOKEN_CONTEXT } = this.CONSTANTS
    return httpContext.get(RESPONSE_TOKEN_CONTEXT) || ''
  }

  // User
  setUser (user = {}) {
    const { USER_CONTEXT } = this.CONSTANTS
    return httpContext.set(USER_CONTEXT, user)
  }

  getUser () {
    const { USER_CONTEXT } = this.CONSTANTS
    return httpContext.get(USER_CONTEXT) || {}
  }

  // API User
  setApiUser (apiUser = {}) {
    const { API_USER_CONTEXT } = this.CONSTANTS
    return httpContext.set(API_USER_CONTEXT, apiUser)
  }

  getApiUser () {
    const { API_USER_CONTEXT } = this.CONSTANTS
    return httpContext.get(API_USER_CONTEXT) || {}
  }

  // UID
  setUid (uid = '') {
    const { UID_CONTEXT } = this.CONSTANTS
    return httpContext.set(UID_CONTEXT, uid)
  }

  getUid () {
    const { UID_CONTEXT } = this.CONSTANTS
    return httpContext.get(UID_CONTEXT) || ''
  }

  setReqResBodyLog (reqResBodyLog = true) {
    const { REQ_RES_BODY_LOG_CONTEXT } = this.CONSTANTS
    return httpContext.set(REQ_RES_BODY_LOG_CONTEXT, reqResBodyLog)
  }

  getReqResBodyLog () {
    const { REQ_RES_BODY_LOG_CONTEXT } = this.CONSTANTS
    return httpContext.get(REQ_RES_BODY_LOG_CONTEXT)
  }
}
