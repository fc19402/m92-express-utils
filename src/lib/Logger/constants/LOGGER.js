'use strict'

const LEVELS = {
  FATAL: 'FATAL',
  ERROR: 'ERROR',
  WARN: 'WARN',
  INFO: 'INFO',
  DEBUG: 'DEBUG',
  TRACE: 'TRACE'
}

const LEVEL_RANK = {
  [LEVELS.FATAL]: 0,
  [LEVELS.ERROR]: 1,
  [LEVELS.WARN]: 2,
  [LEVELS.INFO]: 3,
  [LEVELS.DEBUG]: 4,
  [LEVELS.TRACE]: 5
}

const LEVEL_COLORS = {
  [LEVELS.FATAL]: 'magentaBright',
  [LEVELS.ERROR]: 'redBright',
  [LEVELS.WARN]: 'yellow',
  [LEVELS.INFO]: 'greenBright',
  [LEVELS.DEBUG]: 'blueBright',
  [LEVELS.TRACE]: undefined
}

const MODES = {
  CONSOLE: 'CONSOLE',
  CONSOLE_VERBOSE: 'CONSOLE_VERBOSE'
}

const LOGGER = {
  LEVELS,
  LEVEL_RANK,
  LEVEL_COLORS,
  MODES
}

export default LOGGER
