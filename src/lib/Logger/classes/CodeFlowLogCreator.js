'use strict'

import LOG_TYPES from '../constants/LOG_TYPES'

export default function CodeFlowLogCreator (BaseLog) {
  return class CodeFlowLog extends BaseLog {
    constructor (level = '', message = '', data) {
      super(LOG_TYPES.CODE_FLOW_LOG, level)

      const msg = message || ''
      this.message = (typeof msg === 'string' || msg instanceof String) ? msg.toString() : JSON.stringify(msg)
      this.data = data || {}
    }
  }
}
