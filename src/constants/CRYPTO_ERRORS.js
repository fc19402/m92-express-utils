'use strict'

import ERROR_CLASSIFICATIONS from './ERROR_CLASSIFICATIONS'

const CRYPTO_ERRORS = {
  INVALID_PASSPHRASE_FUNC: {
    statusCode: 500,
    message: "Provided 'getPassphrase' Option is not a function",
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  },
  INVALID_PUBLIC_KEY_FUNC: {
    statusCode: 500,
    message: "Provided 'getPublicKeyArmored' Option is not a function",
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  },
  INVALID_PRIVATE_KEY_FUNC: {
    statusCode: 500,
    message: "Provided 'getPrivateKeyArmored' Option is not a function",
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  },
  PAYLOAD_ENCRYPTION: {
    statusCode: 500,
    message: 'Error Encrypting Payload',
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  },
  PAYLOAD_DECRYPTION: {
    statusCode: 500,
    message: 'Error Decrypting Payload',
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  },
  CREDENTIAL_DECRYPTION: {
    statusCode: 500,
    message: 'Error Decrypting Credentials',
    classification: ERROR_CLASSIFICATIONS.CRYTOGRAPHIC
  }
}

export default CRYPTO_ERRORS
